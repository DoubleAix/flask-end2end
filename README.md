# Flask-End2End
> Flask-End2End is the extension of the Flask framework.** This extension allows developers to save time without considering how to implement the feature of recording audit logs & tracing the journey of the transaction. **

## How it Works
### Trace Log
  * Add following headers to the request object or the response object automatically in order to trace the whole journey of the transaction.
  * Fields for tracing the whole journey, as follows:
    * Transaction-Id
    * Transaction-Seq
    * System-Name
  
```script
{ "Host": "127.0.0.1:10102", "User-Agent": "python-equests/2.18.4", "Accept-Encoding": "gzip, deflate","Accept": "*/*", "Connection": "keep-alive",
 "Transaction-Id": "2752d5dd-bc58-4c61-97d3-38b788c20615", "Transaction-Seq": "0", "System-Name": "web_agent","Content-Length": "0" }
```

  * Fetch above headers from the source request object for logging & requests module usage later.
  * Add above headers automatically to fields of the format of the logger.
	
```script
## trace log format using string as input as follows:
{"message": "message you want to record.", "@timestamp": "2019-02-20T03:35:47.277276Z", "level_name": "INFO", "transaction_id": "eb30a799-418a-4f21-a437-12b11f23495c", "transaction_seq": "0", "source_user_id": "Hi Handsome", "system_id": "web_agent"}
```

```script
## trace log format using dict as input as follows:
{"message": "null", "type":"custom", "@timestamp": "2019-02-20T03:35:47.277276Z", "level_name": "INFO", "transaction_id": "eb30a799-418a-4f21-a437-12b11f23495c", "transaction_seq": "0", "source_user_id": "Hi Handsome", "system_id": "web_agent"}
```
  *  Extend original requests module feature - automatically add above headers to each request object in order to trace but change some headers as follows:
     * Change System-Name header to the system name of your own.
     * Change Transaction-Seq header to the original value of Transaction-Seq plus 1.  
  
### Audit Log
  * Fields recording behaviors of the user, as follows:
    1. User (user_id)
    2. EndPoint (path)
	1. TimeStamp (@timestamp)
	2. Source Loaction (user_ip)
	3. HTTP Method (method)
   
```script
{ "message": null, "@timestamp": "2019-02-20T03:35:47.253411Z", "user_id": "Pretty", "method": "POST", "user_ip": "127.0.0.1", "path": "/" }
```

## Installation

```bash
cd [your project]
source /ods/analysis/[AD account]/project/venv/bin/activate
pip install -i file:///ods/python_packages/simple flask_end2end
```

## Example
The example code as follows:
```python
from flask import Flask
#import requests                              # comment "import requests" statement
app=Flask(__name__)

from flask_end2end.end2end import End2End, requests
app.config['SYSTEM_NAME'] = 'system_name'     # (necessary) system name
app.config['PATH'] = './'                     # (optional) the path of the trace log folder (Default: /ods/odsai/logs/flask/)
app.config['AUDIT_PATH'] = './'               # (optional) the path of the audit log folder (Default: /ods/odsai/logs/audit/)
app.config['FIRST'] = True                    # (optional) enable the functionality of recording audit log or not (Default: False)
e2e = End2End(app)                            # initialize it with an application object
											  
@e2e.required_login                           # using decorator to fetch user_id
def get_user_name():						  
    return "user_id"

@app.route("/", methods = ['GET', 'POST','PUT','DELETE'])
def hello_world():
    url = 'http://XXXXXX.com'
    app.logger.info('message you want to record.')
    message_dict = { "type":"custom" }
    #(optional) It is recommended to use the dictionary data type to add additional information.
	app.logger.info(dict(message_dict))
    requests.get(url)
    return 'Hello, World!'
    
if __name__ == '__main__':
    app.run()

```