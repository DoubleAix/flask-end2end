from datetime import datetime
import dns.resolver
import dns.reversename
from pythonjsonlogger import jsonlogger
from flask import request
import logging


def _newlookup(ipin):
        my_resolver = dns.resolver.Resolver()
        ipaddy = ipin
        ipaddy = dns.reversename.from_address(ipin)
        try:
            hostname = str(my_resolver.query(ipaddy,"PTR")[0])
        except:
            my_resolver.nameservers = ['10.42.1.40']
            hostname = str(my_resolver.query(ipaddy,"PTR")[0])
            
        return hostname


def _build_timestamp():
    return datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        ''' # this doesn't use record.created, so it is slightly off'''
        now = _build_timestamp() #datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        log_record['@timestamp'] = now
        log_record['level_name'] = record.levelname
        log_record['transaction_id'] = request._transaction_id 
        log_record['transaction_seq'] = request._transaction_seq
        if request._source_system!='':
            log_record['source_system'] = request._source_system 
        log_record['system_id'] = request._current_system_name
        
def create_audit_log(logger, first_flag = False ,get_name = '', response= ''):
    status_code = str(response.status_code)
    status_as_integer = status_code[0]
    if status_as_integer == '2':
        status ='success'
    elif status_as_integer == '4':
        status ='failure'
    else:
        status_code = 'UNKNOW'
    
    log_dict={}
    log_dict['@timestamp'] = _build_timestamp()
    if first_flag:
        log_dict['user_id'] = get_name#self.get_ad()
    else:
        log_dict['source_system_name'] = request._source_system 
    log_dict['method'] = request.method
    log_dict['user_ip'] = request.environ['REMOTE_ADDR']
    log_dict['path']  = request.path
    log_dict['hostname'] = _newlookup(request.environ['REMOTE_ADDR'])
    log_dict['status_code'] = status_code
    log_dict['status'] = status
    logger = logging.getLogger('audit')
    logger.info(log_dict)