from flask import request
from uuid import uuid4

def _add_headers(f):
    """This decorator adds the headers"""
    def decorated_function(*args, **kwargs):
        add_headers={
            'Transaction-ID': request._transaction_id,
            'Transaction-Seq': request._transaction_seq,
            'System-Name': request._current_system_name
        }
         
        if 'headers' in kwargs:
            kwargs['headers'].update(add_headers)
        else:
            kwargs['headers'] = add_headers
                
        return f(*args, **kwargs)
    return decorated_function


def _transactionmetadata(system_name):
        '''take some info to headers from request'''
        if request.headers.get('Transaction-ID') and request.headers.get('Transaction-Seq'):
            request._transaction_id = request.headers.get('Transaction-ID')
            request._transaction_seq = str(int(request.headers.get('Transaction-Seq'))+1) 
        else:
            if request.headers.get('Transaction-ID'):
                request._transaction_id = request.headers.get('Transaction-ID')
            else:
                request._transaction_id = str(uuid4())

            request._transaction_seq = '0'
        
        if request.headers.get('System-Name'):
            request._source_system = request.headers.get('System-Name') 
        else:
            request._source_system = ''
        
        request._current_system_name = system_name #self.system_name
        
def _add_response_headers(response):
    """This function adds the headers passed in to the response"""
    response_headers = {'Transaction-ID': request._transaction_id}
    for header, value in response_headers.items():
        response.headers[header] = value
    return response