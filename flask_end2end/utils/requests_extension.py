from requests import get, put, post, delete, options, head, patch
import requests

from flask_end2end.utils.header_alter import _add_headers

@_add_headers
def get(url, params=None, **kwargs):
    return requests.get(url, params=params, **kwargs)
# get = _add_headers(requests.get)

@_add_headers
def put(url, data=None, **kwargs):
    return requests.put(url, data=data, **kwargs)
    
@_add_headers
def delete(url, **kwargs):
    return requests.delete(url, **kwargs)

@_add_headers
def post(url, data=None, json=None, **kwargs):
    return requests.post(url, data=data, json=json, **kwargs)

@_add_headers
def options(url, **kwargs):
    return requests.options(url, **kwargs)

@_add_headers
def head(url, **kwargs):
    return requests.head(url, **kwargs)

@_add_headers
def patch(url, data=None, **kwargs):
    return requests.patch(url, data=data, **kwargs)

# def fun(funt, url, params=None, data=None, json=None, **kwargs):
#     try:
#         return requests.funt(url, params=params, data=None, json=None, **kwargs)
#     except:
#         return funt(url, params=params, data=None, json=None, **kwargs)

# #app.before_request(overwrite_requests)
# def overwrite_requests(**kwargs):
#     """This decorator adds the headers"""
#         add_headers={
#             'Transaction-ID': request._transaction_id,
#             'Transaction-Seq': request._transaction_seq,
#             'System-Name': request._current_system_name
#         }
         
#         if 'headers' in kwargs:
#             kwargs['headers'].update(add_headers)
#         else:
#             kwargs['headers'] = add_headers
        
#         response = fun(f, url, params=None, data=None, json=None, **kwargs)
#         return response

