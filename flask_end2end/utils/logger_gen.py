import os
import logging
from logging.handlers import TimedRotatingFileHandler
      
def create_dir_if_not_exist(dirName): 
    '''Check if a Directory is empty and also check exceptional situations.'''    
    if os.path.exists(dirName) is False and os.path.isdir(dirName) is False:
            os.mkdir(dirName)
        
class Create_Logger(object):
    def __init__(self, logger_name,logger_path):
        self.logger_name = logger_name
        self.logger_path = logger_path
    
    def init_logger_folder(self):
        create_dir_if_not_exist(self.logger_path)
        
    def create_logger(self):
        logger = logging.getLogger(self.logger_name)
        return logger
    
    def set_logger(self, logger, logger_path, logger_filename, logger_format):
        logger_filename = logger_filename + '.log'
        log_path = os.path.join(logger_path, logger_filename)
        handler_login = TimedRotatingFileHandler(log_path, when='midnight', interval=5)
        
        handler_login.setFormatter(logger_format)
        logger.addHandler(handler_login)
        logger.setLevel(logging.INFO)
        return logger
    
    def add_col(self, logger, item):#logging.getlogger()
        if 'dict' in str(type(item)):
            return logging.LoggerAdapter(logger, item)
        else:
            raise TypeError('The data type shoud be dictionary.')