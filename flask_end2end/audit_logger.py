from flask_end2end.utils.logger_gen import Create_Logger
from flask_end2end.utils.formatter_for_logger import create_audit_log

def fake_name():
    return "UNKNOWN"

class Create_Audit_Logger(Create_Logger):
    def __init__(self, logger_name, logger_path, user_name):
        super(Create_Audit_Logger, self).__init__(logger_name, logger_path)
        self.user_name = user_name
    
    def get_ad(self):
        return self.user_name()  
    
    def first_user_id(self):
        return {'source_user_id' : self.get_ad()}