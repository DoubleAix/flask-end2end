from functools import wraps
import os
import logstash

from flask_end2end.utils.logger_gen import Create_Logger
from flask_end2end.utils.formatter_for_logger import CustomJsonFormatter, jsonlogger, create_audit_log
from flask_end2end.utils.header_alter import _transactionmetadata, _add_response_headers
from flask_end2end.audit_logger import Create_Audit_Logger, fake_name
import flask_end2end.utils.requests_extension as requests

class End2End(object):
    def __init__(self, app = None):
        if app:
            self.init_app(app)    
    
    def init_app(self, app):
        self.app = app
        '''init config'''
        self.system_name = app.config['SYSTEM_NAME']
        if 'PATH' in app.config:
            self.path = app.config['PATH'] 
        else:
            self.path = '/ods/odsai/logs/flask/'
        if 'AUDIT_PATH' in app.config:
            self.audit_path = app.config['AUDIT_PATH'] 
        else:
            self.audit_path = '/ods/odsai/logs/audit/'
        if 'FLAG' in app.config:
            self.flag = app.config['FLAG']
        else:
            self.flag = False   
        if 'FIRST' in app.config:
            self.first_flag = app.config['FIRST']
        else:
            self.first_flag = False
            
        if 'LOGSTASH' in app.config:
            self.logstash = app.config['LOGSTASH']
        else:
            self.logstash = False
        
        if self.logstash:
            if 'HOST'  and 'PORT' in app.config:
                self.host = app.config['HOST']
                self.port = app.config['PORT']
                
        if self.path[-1]!='/':
            self.path +='/'
               
        if self.audit_path[-1]!='/':
            self.audit_path +='/'
        
        self.log_path = os.path.join(self.path, self.system_name)
        self.log_audit_path = os.path.join(self.audit_path, self.system_name)
        
        app.before_request(self._get_transactionmetadata)
        
        '''init logger object'' ''''modify app logger object'''
        trace = 'trace'
        self.trace_gen = Create_Logger(trace, self.log_path)
        self.trace_gen.init_logger_folder()
#         self.trace_gen.create_logger() #flask app.logger is exist
        app.logger = self.trace_gen.set_logger(app.logger, self.log_path, trace,CustomJsonFormatter('(message)'))
        
        if self.logstash:
            print('host type is {} :{}'.format(type(self.host), self.host))
            print('port type is {} :{}'.format(type(self.port), self.port))
            handler_logstash = logstash.LogstashHandler(self.host, self.port, version=0)
#             handler_logstash.setFormatter()#can't flexible
            print(handler_logstash)
            app.logger.addHandler(handler_logstash)
        
        if self.first_flag:
            audit = 'audit'
            self.audit_gen = Create_Audit_Logger(audit, self.log_audit_path, fake_name)
            self.audit_logger = self.audit_gen.create_logger()
            self.audit_logger = self.audit_gen.set_logger(self.audit_logger, self.log_audit_path, audit, jsonlogger.JsonFormatter())
            app.before_request(self._first_user_id)
            app.after_request(self._create_audit_log)
        
        app.after_request(_add_response_headers)
        
    def _first_user_id(self):
        item = self.audit_gen.first_user_id()
        self.app.logger = self.audit_gen.add_col(self.app.logger, item)

    def _get_transactionmetadata(self):
        _transactionmetadata(self.system_name)
        
    def _create_audit_log(self, response):            
        create_audit_log(self.audit_logger, self.first_flag, self.audit_gen.get_ad(), response)
        return response
    
    def required_login(self,f):
        if self.first_flag:
            self.audit_gen.user_name = f
        """
        Required user to login
        """
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)
        return f