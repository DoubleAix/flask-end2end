import setuptools

__version__ = '0.0.6'	

install_requires=['markdown',
		  'requests>=2.18.4',
		  'flask>=1.0.2',
		  'python-json-logger>=0.1.1',
		  'uuid>=1.30',
		  'dnspython>=1.16',
		  'python-logstash>=0.4.6']
	  
setuptools.setup(
    name="flask_end2end",
    version=__version__,
    description="trace and audit logging",
	packages=setuptools.find_packages(exclude=["tests", "tools", "docs", "contrib"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
		"Topic :: Flask Extension :: logging"
    		],
	install_requires=install_requires)
